import { jmcDB } from "@/utils/mongo";
import { ObjectId } from "bson";

export default async (req, res) => {
    const request = JSON.parse(req.body);
    jmcDB
        .collection("jime")
        .deleteOne(
            {
                _id: new ObjectId(request.uuid),
            }
        )
        .then(res.status(200).send("Success"))
        .catch(res.status(400).send("Please input a valid UUID."));
};
